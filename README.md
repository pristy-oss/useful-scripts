# Pristy Useful Scripts

This project contains scripts (mainly shell scripts) use by the Jeci team.

## Install

You can git clone this project in `$HOME/.local/share/usedul-scripts` then symlink script you need in your path.

``` shell
git clone git@gitlab.com:pristy-oss/useful-scripts.git "$HOME/.local/share/useful-scripts"

ln -s "$HOME/.local/share/useful-scripts/pristy-release.sh" "$HOME/.local/bin/pristy-release"

export PATH="$PATH:$HOME/.local/bin"
```

## Auto-Install

We made a script that did it for you.

```shell
source <(curl -fsSL https://gitlab.com/pristy-oss/useful-scripts/-/raw/stable/useful-scripts.sh)
```

It will git clone the current project in `$HOME/.local/share/useful-scripts` then create a symlink to `useful-scripts.sh`

Then you can install all available scripts

```shell
useful-scripts install --all 
```

or choose which scripts to install

```shell
useful-scripts list

useful-scripts install pristy-release
```

## Update

```shell
useful-scripts update
```

## Uninstall

```shell
useful-scripts uninstall --all
rm "$HOME/.local/bin/useful-scripts"
rm -rf "$HOME/.local/share/useful-scripts"
```