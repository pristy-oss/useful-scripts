#!/usr/bin/env bash
#
# Script Name : docker-ps-status
# Description : Print status of all docker containers
# Author      : Jérémie Lesage, Jeci <https://jeci.fr/>
# Dependency  : https://github.com/charmbracelet/gum

docker ps -a --format 'table {{ .ID }}\t{{ .Names }}\t{{ .Status }}'
