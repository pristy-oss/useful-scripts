#!/usr/bin/env bash
#
# Script Name : pristy-release
# Description : Script create and publish new release of an nodejs software
# Author      : Jérémie Lesage, Jeci <https://jeci.fr/>
# Dependency  : https://github.com/charmbracelet/gum
#
# shellcheck disable=SC2001

function echoerr() {
  echo "[!] $@" 1>&2
}

function check_dependencies() {
	if ! command -v gum >/dev/null 2>&1; then
		echoerr "Gum is missing - https://github.com/charmbracelet/gum"
		exit 1
	fi
}

check_dependencies

shopt -s expand_aliases
[[ "$TRACE" ]] && set -x

alias msg='gum style --border double --align center --width 50 --margin "1 2" --padding "1 2"'
alias gun_spin='gum spin --spinner dot --title'

gun_spin "Load NVM" \
	-- nvm use
gun_spin "npm install" \
	-- npm install
gun_spin "Run Lint" \
	-- npm run lint

if [[ $(git status --short | sed q1) ]]; then
	msg "Repo is not clean" "" "$(git status --short)"
	exit 1
fi

msg "This Script will merge develop branch to stable, then create git tag" "" "git co develop
git pull
git co stable
git merge --ff-only develop
"

gum confirm || exit 1

gun_spin "Checkout develop" \
	-- git switch develop
gun_spin "Pull develop" \
	-- git pull
gun_spin "Checkout stable" \
	-- git switch stable
gun_spin "Merge develop to stable" \
	-- git merge --ff-only develop

CURENT_VERSION=$(sed -n '/"version":/ s/.*"\([0-9][0-9.]*\)",/\1/ p' package.json)

# Patch version
_patch=$(echo "$CURENT_VERSION" | sed 's/.*\([0-9][0-9]*\)$/\1/')
_base=$(echo "$CURENT_VERSION" | sed 's/\(.*\)\([0-9][0-9]*\)$/\1/')
_minor=$(echo "${_base}" | sed 's/[0-9]*\.\([0-9][0-9]*\)\.$/\1/')
_major=$(echo "$CURENT_VERSION" | sed 's/^\([0-9][0-9]*\).*$/\1/')

LAST_VERSION="$(git describe --tags --abbrev=0)"
# shellcheck disable=SC2034  # Unused variables left for readability
PATCH_VERSION="${_base}$((_patch + 1))"
MINOR_VERSION="${_major}.$((_minor + 1)).0"

msg "Last Version Released is $LAST_VERSION" "" "What version do you want to release?"

NEW_VERSION=$(gum choose "$CURENT_VERSION" "$MINOR_VERSION")
_next_base=$(echo "$NEW_VERSION" | sed 's/\(.*\)\([0-9][0-9]*\)$/\1/')
_next_patch=$(echo "$NEW_VERSION" | sed 's/.*\([0-9][0-9]*\)$/\1/')
NEXT_VERSION="${_next_base}$((_next_patch + 1))"

msg "We will release $NEW_VERSION" "Next version will be $NEXT_VERSION"

gum confirm || exit 1

sed -i '/"version":/ s/[0-9][0-9.]*/'"$NEW_VERSION"'/' package.json
GIT_PAGER='' git diff package.json
gun_spin "Load NVM" \
	-- nvm use
rm -r node_modules/@pristy
gun_spin "npm install" \
	-- npm install
npm shrinkwrap
git add package.json npm-shrinkwrap.json

git config changelog.format '- %s'
git changelog
git add CHANGELOG.md

if [[ $(git status --short | sed q1) ]]; then
	gum confirm "Commit version $NEW_VERSION ?" &&
		git commit -m "version $NEW_VERSION" || exit 1
else
	msg "No change to commit" "Is CHANGELOG.md up to date ?"
fi

gum confirm "Push to private gitlab?" || exit 1

gun_spin "Pushing to private gitlab" \
	-- git push origin

gum confirm "Create git tag ${NEW_VERSION}?" &&
	git tag "$NEW_VERSION" || exit 1

gun_spin "Pushing tag to private gitlab" \
	-- git push origin "tags/${NEW_VERSION}"

gum confirm "Push to gitlab.com?" || exit 1

gun_spin "Pushing gitlab.com" \
	-- git push gitlab.com
gun_spin "Pushing tag to gitlab.com" \
	-- git push gitlab.com "tags/${NEW_VERSION}"

msg "We will create next version $NEXT_VERSION to develop branch"

gum confirm "Create version $NEXT_VERSION" || exit 1

gun_spin "Checkout develop" \
	-- git switch develop
gun_spin "Merge develop to stable" \
	-- git merge --ff-only stable

sed -i '/"version":/ s/[0-9][0-9.]*/'"$NEXT_VERSION"'/' package.json
GIT_PAGER='' git diff package.json
gun_spin "Load NVM" \
	-- nvm use
gun_spin "npm install" \
	-- npm install

git add package.json npm-shrinkwrap.json
git commit -m "init version $NEXT_VERSION"

gun_spin "Git push" \
	-- git push origin
