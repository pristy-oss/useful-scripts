#!/usr/bin/env bash
#
# Script Name : dnf-clean-alll
# Description : Clean all repo and cache in Fedora
# Author      : Jérémie Lesage, Jeci <https://jeci.fr/>
# Dependency  : https://github.com/charmbracelet/gum

sudo dnf autoremove
sudo dnf clean all --enablerepo=\*
sudo dnf system-upgrade clean
sudo pkcon refresh force -c -1
