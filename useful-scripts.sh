#!/usr/bin/env bash
#
# Script Name : useful-scripts
# Description : Script to manage useful-scripts
# Usage       : useful-scripts install --all
# Author      : Jérémie Lesage, Jeci <https://jeci.fr/>

set -o pipefail

if [ -z "$HOME" ]; then
	HOME=$(awk -F : '/^'"$(id -un)"':/ {print $6}' /etc/passwd)
fi

SCRIPT_HOME="$HOME/.local/share/useful-scripts"
declare -r ACTION="$1"

function echoerr() {
	echo "[!] $@" 1>&2
}

function check_dependencies() {
	if ! command -v git >/dev/null 2>&1; then
		echoerr "Git is missing"
		exit 1
	fi
}

function clone_project() {
	echo -e "Clone pristy-oss/useful-scripts.git"
	git clone git@gitlab.com:pristy-oss/useful-scripts.git "${SCRIPT_HOME}"

	echo -e "Update pristy-oss/useful-scripts.git"
	ln -s "${SCRIPT_HOME}/useful-scripts.sh" $HOME/.local/bin/useful-scripts
}

function update_project() {
	echo -e "Update pristy-oss/useful-scripts.git"
	cd "${SCRIPT_HOME}"
	git switch stable
	git pull
}

function install_all() {
	find "${SCRIPT_HOME}" -maxdepth 1 -type f -name "*.sh" -not -name "useful-scripts.sh" -printf "%f" -quit |
		sed "s/\.sh$//" |
		xargs -r -I\{\} ln -s "$HOME/.local/share/useful-scripts/{}.sh" "$HOME/.local/bin/{}"
}

function uninstall_all() {
	find "${SCRIPT_HOME}" -maxdepth 1 -type f -name "*.sh" -not -name "useful-scripts.sh" -printf "%f" -quit |
		sed "s/\.sh$//" |
		xargs -r -I\{\} rm "$HOME/.local/bin/{}"
}

function install_script() {
	if [ "--all" == "$1" ]; then
		echo -e "-- Install all scripts -- \n"
		install_all
		exit 0
	fi

	echo -e "-- Install $1 -- \n"
	find "${SCRIPT_HOME}" -maxdepth 1 -type f -name "$1.sh" -not -name "useful-scripts.sh" -printf "%f" -quit |
		grep "." |
		sed "s/\.sh$//" |
		xargs -r -I\{\} ln -s "$HOME/.local/share/useful-scripts/{}.sh" "$HOME/.local/bin/{}"
	RESULT=$?
	if [ $RESULT -eq 0 ]; then
		echo -e "-- Script $1 installed --"
	else
		echoerr "Script $1 is not found"
	fi
}

function uninstall_script() {
	if [ "--all" == "$1" ]; then
		echo -e "-- Install all scripts -- \n"
		uninstall_all
		exit 0
	fi

	echo -e "-- Uninstall $1 -- \n"
	find "$HOME/.local/bin/" -maxdepth 1 -type l -name "$1" -not -name "useful-scripts" -print -quit |
		grep "." |
		xargs -r rm
	RESULT=$?
	if [ $RESULT -eq 0 ]; then
		echo -e "-- Script $1 uninstalled --"
	else
		echoerr "Script $1 is not found"
	fi
}

function list_scripts() {
	find "${SCRIPT_HOME}" -maxdepth 1 -type f -name "*.sh" -printf "%f\t(%Cx)\n" | sed 's/.sh$//'
}

function info_script() {
	find "${SCRIPT_HOME}" -maxdepth 1 -type f -name "$1.sh" -print -quit | \
	xargs sed -n '3,/^$/ s/^#// p' | \
	grep -v "shellcheck disable"
}

function _usage() {
	echo "Usage: useful_scripts ACTION [name]"
	echo "  actions:"
	echo "    install NAME   : Install a script"
	echo "    uninstall NAME : Uninstall a script"
	echo "    list           : List all available scripts"
	echo "    info NAME      : Print description of the script"
	echo "    update         : Update all scripts (default)"
}

check_dependencies

if [[ -z "$ACTION" ]]; then
	if [ ! -d "${SCRIPT_HOME}" ]; then
		clone_project
	else
		update_project
	fi
	exit 0
fi

case "$ACTION" in
install)
	shift
	install_script "$@"
	;;
uninstall)
	shift
	uninstall_script "$@"
	;;
update)
	update_project
	;;
list)
	list_scripts
	;;
info)
	shift
	info_script "$@"
	;;
*)
	_usage
	exit 0
	;;
esac
