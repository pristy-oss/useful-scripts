#!/usr/bin/env bash
#
# Script Name : push-tag-prod
# Description : Git pull staging branch, create date tag, push to origin/prod branch
#             : This script is use to publish our website (hugo) with a timestamp
# Author      : Jérémie Lesage, Jeci <https://jeci.fr/>
# Dependency  : git

function check_dependencies() {
	if ! command -v git >/dev/null 2>&1; then
		echoerr "Git is missing"
		exit 1
	fi
}

git switch staging
git pull
TAG="prod-$(date +%y%m%d%H%M)"
git tag "$TAG"
git push origin "tags/$TAG"
git push origin HEAD:prod
